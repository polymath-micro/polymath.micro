# Changelog

All notable changes to this project will be documented in this file.

## [1.0] - 2020-06-18

### Changed
- Fixed `README.md` to include proper installation instructions and instructions for running each demo
- Fixed `memory_enhance.py` and `lenet5.py` to be located in the root directory, and added a function for printing graph nodes in memory_enhance.
- Removed `demo/` directory to avoid import errors when using relative paths
- Fixed `__init__.py` to include error message when running PolyMath without TVM.
- Removed De-anonymizing text file
