import polymath as pm
import numpy as np

class fft(pm.Template):
    def define_graph(self, x, y, n, **kwargs):
        n1 = pm.index(0, n-1)
        n2 = pm.index(0, n-1)
        m = pm.temp(name="m", shape=(n,n))

        m[n1, n2] = pm.exp(-2j * np.pi*n1*n2/n)
        y[n1] = pm.sum([n2], (m[n1, n2]*x[n2]))

class lreg(pm.Template):
    def define_graph(self, x, w, y, n, **kwargs):
        i = pm.index(0, n-1)
        y.write(pm.sigmoid(pm.sum([i], x[i]*w[i])))

class update_ctrl_model(pm.Template):
    def define_graph(self, ctrl_prev, g, ctrl_mdl, ctrl_sgnl, h, b, s, **kwargs):
        i = pm.index(0, b-2, name="ucm_i")
        j = pm.index(0, s-1, name="ucm_j")
        ctrl_sgnl[j] = ctrl_mdl[(h*j).set_name("h*j")]
        ctrl_mdl[(h-1)*j] = 0
        ctrl_mdl[i] = ctrl_prev[(i+1)*h] - g[(i+1)*h]

class mvmul(pm.Template):
    def define_graph(self, a, b, c, n, m, **kwargs):

        i = pm.index(0, n-1, name="mvmul_i")
        j = pm.index(0, m-1, name="mvmul_j")
        c[j] = pm.sum([i], a[j,i]*b[i])

class predict_brain_state(pm.Template):
    def define_graph(self, brain_state, ctrl_mdl, P, H, pred, a, b, c, **kwargs):
        i = pm.index(0, a-1, name="pbs_i")
        j = pm.index(0, b-1, name="pbs_j")
        k = pm.index(0, c-1, name="pbs_k")
        pred[k] = pm.sum([i], P[k,i]*brain_state[i]) + pm.sum([j], H[k,j]*ctrl_mdl[j])

class compute_ctrl_gradient(pm.Template):
    def define_graph(self, brain_state, ctrl_mdl, brain_state_ref, HQ_g, R_g, g,a,b, **kwargs):

        i = pm.index(0, b-1, name="b_i")
        j = pm.index(0, a-1, name="a_j")
        P_g = pm.state(name="P_g", shape=(a,))
        H_g = pm.state(name="H_g", shape=(a,))
        err = pm.state(name="err", shape=(b,))
        err[i] = brain_state_ref[i] - brain_state[i]

        mvmul(HQ_g, err, P_g, b, a)
        mvmul(R_g, ctrl_mdl, H_g, a, a)
        g[j] = P_g[j] + H_g[j]

def print_graph_nodes(graph, indents=0):
    indent_str = '\t'*indents
    for node_name, node in graph.nodes.items():
        print(f"{indent_str}{node_name} - {node.op_name}:")
        if len(node.nodes.items()) > 0:
            print_graph_nodes(node, indents+1)

def main():
    m = 128
    n = 384
    c = 128
    s = 3
    with pm.Node(name="memory_enhance") as graph:
        signal = pm.input(name="signal", shape=(m,))
        fft_out = pm.output(name="fft_out", shape=(m,))
        lreg_weights = pm.input(name="lreg_weights", shape=(m,))
        lreg_out = pm.output(name="lreg_out", shape=(1,))
        lreg_refs = pm.state(name="lreg_refs", shape=(n,))
        i = pm.index(1, n-1)
        lreg_refs[i] = 0

        ctrl_mdl = pm.state(name="ctrl_mdl", shape=(n,))
        brain_state_pred = pm.output(name="brain_state_pred", shape=(c,))
        g = pm.output(name="g", shape=(n,))
        P = pm.parameter(name="P", shape=(c,m))
        H = pm.parameter(name="H", shape=(c,n))
        HQ_g = pm.parameter(name="HQ_g", shape=(n,c))
        R_g = pm.parameter(name="R_g", shape=(n,n))
        ctrl_sgnl = pm.output(name="ctrl_sgnl", shape=(s,))
        i = pm.index(1, n-1)

        fft(signal, fft_out, m)
        lreg(fft_out, lreg_weights, lreg_out, m)
        lreg_refs[i] = lreg_refs[i-1]
        lreg_refs[(0,)] = lreg_out

        predict_brain_state(fft_out, ctrl_mdl, P, H, brain_state_pred, m, n, c)
        compute_ctrl_gradient(brain_state_pred, ctrl_mdl, lreg_refs, HQ_g, R_g, g, n, c)
        update_ctrl_model(ctrl_mdl, g, ctrl_mdl, ctrl_sgnl, c, n, s)
    # Print graph nodes
    print_graph_nodes(graph)

if __name__ == "__main__":
    main()